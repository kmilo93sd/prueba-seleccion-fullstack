const router = require("express").Router();

router.get("/characters", (request, response) =>
  response.json({ data: [{ id: 1, name: "Jhon" }, { id: 2, name: "Arya" }] })
);

router.get("/characters/:id", (request, response) =>
  response.json({ data: { id: 1, name: "Jhon" } })
);

module.exports = router;
