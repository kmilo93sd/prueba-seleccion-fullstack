const Character = require("./../models/Character");

const all = async () => {
  const characters = await Character.find({});
  return characters.map(({ _doc: { _id, ...character } }) => {
    return {
      id: _id,
      ...character
    };
  });
};

const byId = async characterId => {
  const result = await Character.findById(characterId);

  if (!result) {
    return result;
  }

  const {
    _doc: { _id, ...character }
  } = result;
  return {
    id: _id,
    ...character
  };
};

module.exports = { all, byId };
