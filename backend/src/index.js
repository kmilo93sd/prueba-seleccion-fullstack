const express = require("express");
const example = express();

example.use("/api", require("./routes"));

module.exports = example;
