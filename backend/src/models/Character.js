const mongoose = require("./../persistence/Mongoose");

const Schema = mongoose.Schema;

const schema = new Schema({
  _id: String,
  name: String,
  slug: String,
  gender: String,
  house: String
});

module.exports = mongoose.model("Character", schema);
